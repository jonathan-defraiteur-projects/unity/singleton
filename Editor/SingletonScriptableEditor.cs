using JonathanDefraiteur.Runtime;

namespace JonathanDefraiteur.Editor
{
    [UnityEditor.CustomEditor(typeof(SingletonScriptable), true)]
    public class SingletonScriptableEditor : AbstractSingletonEditor
    { }
}