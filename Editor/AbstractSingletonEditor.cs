namespace JonathanDefraiteur.Editor
{
    public abstract class AbstractSingletonEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            UnityEditor.EditorGUILayout.HelpBox("SINGLETON", UnityEditor.MessageType.Info);
     
            base.OnInspectorGUI();
        }
    }
}