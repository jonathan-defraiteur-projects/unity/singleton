﻿using UnityEngine;

namespace JonathanDefraiteur.Runtime
{
    /// <summary>
    /// Use for custom editor purpose
    /// </summary>
    public abstract class SingletonBehaviour : MonoBehaviour {}
    
    public abstract class SingletonBehaviour<T> : SingletonBehaviour where T : MonoBehaviour
    {
        protected static T _instance;
        protected static object _lock = new object();
        protected static bool _applicationIsQuitting = false;

        public static T Instance
        {
            get {
                if (_applicationIsQuitting) {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                     "' already destroyed on application quit." +
                                     " Won't create again - returning null.");
                    return null;
                }

                lock (_lock) {
                    if (null != _instance) {
                        return _instance;
                    }
                    
                    T[] objs = FindObjectsOfType<T>();
                    if (objs.Length == 1) {
                        _instance = objs[0];
                    }
                    else {
                        Debug.LogError("[Singleton] You must have at most one " + typeof(T).Name + " in the scene.");
                    }

                    return _instance;
                }
            }
        }

        public static bool HasInstance()
        {
            return null != Instance;
        }

        protected bool SelfRegisterInstance()
        {
            if (null == _instance) {
                _instance = this as T;
                Application.quitting += () => _applicationIsQuitting = true;
                return true;
            } else if (this != _instance) {
                Debug.LogWarning("[Singleton] You must have at most one " + typeof(T).Name + " in the scene." +
                                 "GameObject '" + this.name + "' is destroy.");
                Destroy(gameObject);
            }
            
            return false;
        }
        
        protected void Awake()
        {
            SelfRegisterInstance();
        }
    }
}
