using UnityEngine;

namespace JonathanDefraiteur.Runtime
{
    /// <summary>
    /// Use for custom editor purpose
    /// </summary>
    public abstract class SingletonScriptable : ScriptableObject
    { }
    
    public abstract class SingletonScriptable<T> : SingletonScriptable where T : ScriptableObject
    {
        protected static T _instance;
        protected static object _lock = new object();

        public static T Instance
        {
            get {
                if (null != _instance) {
                    return _instance;
                }

                lock (_lock) {
                    T[] objs = Resources.FindObjectsOfTypeAll<T>();
                    if (objs.Length == 1) {
                        _instance = objs[0];
                    }
                    else {
                        Debug.LogError("[Singleton] You must have at most one " + typeof(T).Name + " in the Resources folder.");
                    }
                    
                    return _instance;
                }
            }
        }

        public static bool HasInstance()
        {
            return null != Instance;
        }
    }
}